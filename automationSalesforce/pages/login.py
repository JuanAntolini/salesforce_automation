from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class SeleniumLogin:

    chrome_driver_path = '/Users/jantolini/PycharmProjects/automationSalesforce/chromedriver'
    title = 'Inicia Sesión'
    driver = webdriver.Chrome(chrome_driver_path)

    def __init__(self, driver):
        self.driver = driver
        self.driver.get("http://claro--testcat.my.salesforce.com")

    def loginExitoso(self):
        email_field = WebDriverWait(self.driver, 20).until(
            ec.visibility_of_element_located((By.ID, "username")))
        email_field.send_keys('user@globalhitss.com.testcat')
        pass_field = WebDriverWait(self.driver, 20).until(
            ec.visibility_of_element_located((By.ID, "password")))
        pass_field.send_keys('pass')
        inicia_sesion = self.driver.find_element_by_id('Login')
        inicia_sesion.click()
        login_ok = self.driver.find_element_by_id('editPage')
        if login_ok:
            print("Ok login exitoso")
            remember_later = WebDriverWait(self.driver,20).until(
                ec.visibility_of_element_located((By.XPATH, "//a[contains(text(), 'Recordarme más tarde')]")))
            remember_later.click()
            pagina_inicio = WebDriverWait(self.driver, 35). until(
                self.driver.current_url == 'https://claro--testcat.lightning.force.com/lightning/page/home'
            )
            print(pagina_inicio)


        else:
            print('ERROR')
