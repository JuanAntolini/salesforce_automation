import os
import unittest
from selenium import webdriver
import pages.login


class SalesforceAutomation(unittest.TestCase):
    """Clase global para inicializar el testing"""

    def setUp(self):
        chrome_driver_path = '/Users/jantolini/PycharmProjects/automationSalesforce/chromedriver'
        self.driver = webdriver.Chrome(chrome_driver_path)
        self.driver.get("http://claro--testcat.my.salesforce.com")

    def testLogin(self):
        main_page = pages.login.SeleniumLogin(self.driver)
        # Busca si la palabra "Salesforce" esta en el titulo
        assert main_page.loginExitoso(), "el titulo salesforce no coincide."
        # Define el texto de busqueda como "Log In to Sandbox"
        main_page.search_text_element = "Log In to Sandbox"

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
